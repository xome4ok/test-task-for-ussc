﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;

// strategy pattern

// strategy client:
namespace test_task
{
    class Sorter
    {
        private ISortStrategy strategy;

        public Sorter(ISortStrategy s)
        {
            strategy = s;
        }

        public IList<T> Sort<T>(IList<T> array) where T : IComparable<T>
        {
            return strategy.Sort<T>(array);
        }
    }

    // every sort strategy must implement this interface
    interface ISortStrategy
    {
        IList<T> Sort<T>(IList<T> array) where T : IComparable<T>;
    }

    // Bubble sort strategy:

    class BubbleSort : ISortStrategy
    {
        public IList<T> Sort<T>(IList<T> array) where T : IComparable<T>
        {
            var copy = array.ToList<T>();

            T temp;
            int i, j;

            for (i = copy.Count - 1; i > 0; i--)
            {
                for (j = 0; j < i; j++)
                {
                    if (copy[j].CompareTo(copy[j + 1]) > 0)
                    {
                        temp = copy[j];
                        copy[j] = copy[j + 1];
                        copy[j + 1] = temp;
                    }
                }
            }

            return copy;
        }
    }
}