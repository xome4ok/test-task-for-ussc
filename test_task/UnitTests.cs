﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Collections;

namespace test_task
{
    [TestClass]
    public class SortTests
    {
        
        [TestMethod]
        public void CanSortValueTypeInt()
        {
            var array = new[] { 1, 5, 7, 3, 5, 2, 7, 8, 4 };
            Sorter sorter = new Sorter(new BubbleSort());

            CollectionAssert.AreEqual(new[] { 1, 2, 3, 4, 5, 5, 7, 7, 8 }, (ICollection) sorter.Sort(array));
        }

        [TestMethod]
        public void CanSortValueTypeString()
        {
            var array = new[] { "a", "c", "d", "b", "f", "e", "g" };
            Sorter sorter = new Sorter(new BubbleSort());

            CollectionAssert.AreEqual(new[] { "a", "b", "c", "d", "e", "f", "g" }, (ICollection) sorter.Sort(array));
        }

        [TestMethod]
        public void CanSortReferenceTypeString()
        {
            var array = new String[] { "a", "c", "d", "b", "f", "e", "g" };
            Sorter sorter = new Sorter(new BubbleSort());

            CollectionAssert.AreEqual(new String[] { "a", "b", "c", "d", "e", "f", "g" }, (ICollection) sorter.Sort(array));
        }

        [TestMethod]
        public void EmptyIsOkayToo()
        {
            var array =  new String[] { };
            Sorter sorter = new Sorter(new BubbleSort());

            CollectionAssert.AreEqual(new String[] { }, (ICollection) sorter.Sort(array));
        }

        [TestMethod]
        public void DoesNotAffectOriginalArray()
        {
            var array = new String[] { "b","c","a" };
            Sorter sorter = new Sorter(new BubbleSort());
            sorter.Sort(array);

            CollectionAssert.AreEqual(new String[] { "b","c","a" }, array);
        }
    }
}
